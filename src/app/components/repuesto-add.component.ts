import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { SweetAlertService } from 'angular-sweetalert-service';

import { MarcasSV } from '../services/marcas.service';
import { Marca } from '../models/marca'

import { Inventario } from '../models/inventario'

import { ProveedoresSV } from '../services/proveedores.service';
import { RepuestosSV } from '../services/repuestos.service';
import { Repuesto } from '../models/repuesto'

import { VehiculosSV } from '../services/vehiculos.service';
import { Vehiculo } from '../models/vehiculo'

import { CategoriasSV } from '../services/categorias.service';
import { Categoria } from '../models/categoria'

@Component({
	selector: 'repuesto-add',
	templateUrl: '../views/repuesto-add.component.html',
	providers: [MarcasSV, CategoriasSV, RepuestosSV, ProveedoresSV, VehiculosSV]
})

export class RepuestoAddComponent{

	public listaMarcas : Marca[];
	public listaCategorias : Categoria[];
	public listaVehiculos : Vehiculo[];


	public inv_id : number;
	public repuesto : Repuesto;
	public vehiculo : Vehiculo;
	public marca : Marca;
	public categoria : Categoria;
	public inventario : Inventario;
	
	constructor(
		private _router: Router,
		private _marcaSV : MarcasSV,
		private _vehiculosSV : VehiculosSV,
		private _repuestoSV : RepuestosSV,
		private _categoriaSV : CategoriasSV,
		private _proveedoresSV : ProveedoresSV,
		private alertService: SweetAlertService
	){
		console.log("Componente de repuestos");
		this.inv_id = null;
		this.vehiculo = new Vehiculo(null, null);
		this.marca = new Marca(null, null);
		this.categoria = new Categoria(null, null);
		this.inventario = new Inventario(parseInt(sessionStorage.getItem('inv_id')));
		this.repuesto = new Repuesto(null, null, null, null, this.categoria, this.marca, this.vehiculo, this.inventario);
	}

	ngOnInit(){
		this.getMarcas();
		this.getCategorias();
		this.getVehiculos();
		this.getProveedor();
	}

	getMarcas(){
		this._marcaSV.getMarcas().subscribe(
			result => {
				console.log(result);
				this.listaMarcas = result;
			},
			error => {
				console.log(<any>error);
			}
			);
	}

	getVehiculos(){
		this._vehiculosSV.getVehiculos().subscribe(
			result => {
				console.log(result);
				this.listaVehiculos = result;
			},
			error => {
				console.log(<any>error);
			}
			);
	}

	getCategorias(){
		this._categoriaSV.getCategorias().subscribe(
			result => {
				console.log(result);
				this.listaCategorias = result;
			},
			error => {
				console.log(<any>error);
			}
			);
	}

	getProveedor(){
		this._proveedoresSV.getProveedor(parseInt(sessionStorage.getItem('user_id'))).subscribe(
			response =>{
				console.log(response.inventario.id);
				sessionStorage.setItem('inv_id', response.inventario.id);
			},
			error =>{
				console.log(<any>error);
			}
		);
	}

	onSubmit(){
		console.log(JSON.stringify(this.repuesto));
		this._repuestoSV.addRepuesto(this.repuesto).subscribe(
			response =>{
				console.log("Guardado:D");
			},
			error =>{
				console.log(<any>error);
				this.alertService.success({
	        			title: '¡El repuesto fue guardado exitosamente!'
	      			});
					this._router.navigate(['/repuestos']);	
			}
		);
	}
}