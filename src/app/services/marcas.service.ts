import  { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class MarcasSV{

	public url = "http://localhost:8080/";

	constructor(
		private _http: Http
	){}

	getMarcas(){
		return this._http.get(this.url+'marcas/all').map(res=> res.json());
	}
}