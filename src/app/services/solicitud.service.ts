import  { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { Solicitud } from '../models/solicitud';


@Injectable()
export class SolicitudSV{

	public url = "http://localhost:8080/";
	// public url = "http://192.168.43.92:8082/";

	constructor(
		private _http: Http
	){}

	addSolicitud(solicitud : Solicitud){
		let newsolicitud = JSON.stringify(solicitud); //Convirtiendo el objeto a JSON	
		let headers = new Headers({
			'Content-type':'application/json'
		});

		return this._http.post(this.url +'solicitudes/add', newsolicitud, {headers:headers})
						 .map(res => res.json());
	}

}
