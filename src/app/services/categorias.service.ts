import  { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class CategoriasSV{

	public url = "http://localhost:8080/";

	constructor(
		private _http: Http
	){}

	getCategorias(){
		return this._http.get(this.url+'categorias/all').map(res=> res.json());
	}
}