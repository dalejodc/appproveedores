/*=============================================================================================
              IMPORTAR DEPENDENCIAS
===============================================================================================*/
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; //para poder hacer binding de inputs
import { HttpModule } from '@angular/http'; //Para usar peticiones
import { MaterializeModule } from 'angular2-materialize';
import { SweetAlertService } from 'angular-sweetalert-service'; //Librería de alertas
import { AppComponent } from './app.component';
import { routing, appRoutingProviders} from "./app.routing"; //Para las rutas

/*=============================================================================================
              IMPORTAR COMPONENTES CREADOS
===============================================================================================*/
import { HomeComponent } from './components/home.component';
import { ErrorComponent } from './components/error.component';
import { BarraNavegacionComponent } from './components/barraNavegacion.component';
import { LoginComponent } from './components/login.component';
import { SolicitudComponent } from './components/solicitud.component';
import { ConfirmacionComponent } from './components/confirmacion.component';
import { RepuestosComponent } from './components/repuestos-list.component';
import { RepuestoAddComponent } from './components/repuesto-add.component';
import { RepuestoEditComponent } from './components/repuesto-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ErrorComponent,
    BarraNavegacionComponent,
    LoginComponent,
    SolicitudComponent,
    ConfirmacionComponent,
    RepuestosComponent,
    RepuestoAddComponent,
    RepuestoEditComponent,
  ],
  imports: [
    BrowserModule,
    MaterializeModule,
    routing,
    FormsModule,
    HttpModule
  ],
  providers: [ appRoutingProviders, SweetAlertService],
  bootstrap: [ AppComponent]
})
export class AppModule { }
