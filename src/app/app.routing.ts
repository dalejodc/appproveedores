/*=============================================================================================
              IMPORTAR DEPENDENCIAS
===============================================================================================*/
import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule} from "@angular/router";

/*=============================================================================================
              IMPORTAR COMPONENTES CREADOS
===============================================================================================*/
import { HomeComponent } from './components/home.component';
import { ErrorComponent } from './components/error.component';
import { LoginComponent } from './components/login.component';
import { SolicitudComponent } from './components/solicitud.component';
import { ConfirmacionComponent } from './components/confirmacion.component';
import { RepuestosComponent } from './components/repuestos-list.component';
import { RepuestoAddComponent } from './components/repuesto-add.component';
import { RepuestoEditComponent } from './components/repuesto-edit.component';


/*=============================================================================================
              ARREGLO DE LAS RUTAS
===============================================================================================*/
const appRoutes: Routes = [
	{path: '', component: LoginComponent}, //Página Home, ruta inicial

	{path: 'home', component: HomeComponent},
	{path: 'login', component: LoginComponent},
	{path: 'solicitud', component: SolicitudComponent},
	{path: 'confirmacion', component: ConfirmacionComponent},
	{path: 'repuestos', component: RepuestosComponent},
	{path: 'repuesto-add', component: RepuestoAddComponent},
	{path: 'repuestos/repuesto-edit/:id', component: RepuestoEditComponent},

	{path: '**', component: ErrorComponent} //Página para errores, cuando la ruta falla
]


//Procedimiento que necesita Angular
export const appRoutingProviders: any[] = [];
/*
-Variable routing de tipo ModuleWithProviders
-Con el método .forRoot le decimos qué arreglo tiene que cargar para las rutas, tiene que ser el
que habíamos creado antes. 
-Agara todas las rutas que le hemos indicado, las introduzca y las inyecte en la configuración de rutas del 
framework */
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);