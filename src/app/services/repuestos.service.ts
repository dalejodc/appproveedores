import  { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { Repuesto } from '../models/repuesto'

@Injectable()
export class RepuestosSV{

	public url = "http://localhost:8080/";

	constructor(
		private _http: Http
	){}

	private headers = new Headers({
		'Content-type': 'application/json',
	});

	getRepuestos(id){
		return this._http.get(this.url+'repuestos/inventario/'+ id).map(res=> res.json());
	}

	getRepuesto(id){
		return this._http.get(this.url+'repuestos/'+ id).map(res=> res.json());
	}

	deleteRepuesto(id){
		return this._http.delete(this.url+'repuestos/'+ id)
						  .map(res => res.json());	
	}

	addRepuesto(repuesto : Repuesto){
		let newrepuesto = JSON.stringify(repuesto); //Convirtiendo el objeto a JSON

		return this._http.post(this.url+'repuestos/', newrepuesto, {headers:this.headers})
						 .map(res => res.json());
	}

	editRepuesto(id, repuesto: Repuesto){
		let newrepuesto = JSON.stringify(repuesto); //Convirtiendo el objeto a JSON		

		return this._http.put(this.url+'repuestos/'+ id, newrepuesto, {headers: this.headers})
						  .map(res => res.json());	
	}
}