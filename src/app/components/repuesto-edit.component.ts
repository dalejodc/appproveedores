import { Component } from '@angular/core';
import { SweetAlertService } from 'angular-sweetalert-service';
import { Router, ActivatedRoute, Params} from '@angular/router';

import { MarcasSV } from '../services/marcas.service';
import { Marca } from '../models/marca'

import { Inventario } from '../models/inventario'

import { ProveedoresSV } from '../services/proveedores.service';
import { RepuestosSV } from '../services/repuestos.service';
import { Repuesto } from '../models/repuesto'

import { VehiculosSV } from '../services/vehiculos.service';
import { Vehiculo } from '../models/vehiculo'

import { CategoriasSV } from '../services/categorias.service';
import { Categoria } from '../models/categoria'

@Component({
	selector: 'repuesto-edit',
	templateUrl: '../views/repuesto-add.component.html',
	providers: [ MarcasSV, CategoriasSV, RepuestosSV, ProveedoresSV, VehiculosSV]
})

export class RepuestoEditComponent{

	public listaMarcas : Marca[];
	public listaCategorias : Categoria[];
	public listaVehiculos : Vehiculo[];


	public inv_id : number;
	public repuesto : Repuesto;
	public vehiculo : Vehiculo;
	public marca : Marca;
	public categoria : Categoria;
	public inventario : Inventario;

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _marcaSV : MarcasSV,
		private _vehiculosSV : VehiculosSV,
		private _repuestoSV : RepuestosSV,
		private _categoriaSV : CategoriasSV,
		// private _proveedoresSV : ProveedoresSV,
		private alertService: SweetAlertService
	){
		this.vehiculo = new Vehiculo(null, null);
		this.marca = new Marca(null, null);
		this.categoria = new Categoria(null, null);
		this.inventario = new Inventario(parseInt(sessionStorage.getItem('inv_id')));
		this.repuesto = new Repuesto(null, null, null, null, this.categoria, this.marca, this.vehiculo, this.inventario);
	}

	ngOnInit(){
		this.getRepuesto();
		this.getMarcas();
		this.getCategorias();
		this.getVehiculos();
	}

	onSubmit(){
		this._route.params.forEach((params: Params)=>{
			let id = params['id'];
			this._repuestoSV.editRepuesto(id, this.repuesto).subscribe(
				response =>{
					this._router.navigate(['/repuestos']);
				},
				error =>{
					this.alertService.success({
	        			title: '¡El repuesto fue editado exitosamente!'
	      			});
					this._router.navigate(['/repuestos']);
				}
			);
		});	
	}

	getRepuesto(){
		this._route.params.forEach((params: Params)=>{
			let id = params['id'];

			this._repuestoSV.getRepuesto(id).subscribe(
				response=>{
					console.log(response);
					this.categoria.nombre = response.categoria.nombre;
					this.marca.nombre = response.marca.nombre;
					this.vehiculo.modelo = response.vehiculo.modelo;
					this.repuesto = response;
				},
				error=>{
					console.log(<any>error);
				}
			);
		});
	}

	getMarcas(){
		this._marcaSV.getMarcas().subscribe(
			result => {
				console.log(result);
				this.listaMarcas = result;
			},
			error => {
				console.log(<any>error);
			}
			);
	}

	getVehiculos(){
		this._vehiculosSV.getVehiculos().subscribe(
			result => {
				console.log(result);
				this.listaVehiculos = result;
			},
			error => {
				console.log(<any>error);
			}
			);
	}

	getCategorias(){
		this._categoriaSV.getCategorias().subscribe(
			result => {
				console.log(result);
				this.listaCategorias = result;
			},
			error => {
				console.log(<any>error);
			}
			);
	}
}