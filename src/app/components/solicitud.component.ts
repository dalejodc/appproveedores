import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';

import { SolicitudSV } from '../services/solicitud.service'
import { Solicitud } from '../models/solicitud';
import { Estado } from '../models/estado';

@Component({
	selector: 'solicitud',
	templateUrl: '../views/solicitud.component.html',
	providers: [SolicitudSV]
})

export class SolicitudComponent{
	public title: string;
	public solicitud: Solicitud;
	public estado: Estado;

	constructor(
		private _solicitudSV : SolicitudSV,
		private _router: Router,
		){
		this.estado = new Estado(1,"", "");
		this.solicitud = new Solicitud(null,null,null,null,null,null, this.estado);
	}

	ngOnInit(){
	}

	onSubmit(){
		console.log(this.solicitud);

		this._solicitudSV.addSolicitud(this.solicitud).subscribe(
			response =>{
				console.log("Guardado:D");
			},
			error =>{
				// console.log("error:",<any>error);
				this._router.navigate(['/confirmacion']);
			}
			);
	}
}