import  { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ProveedoresSV{

	public url = "http://localhost:8080/";

	constructor(
		private _http: Http
	){}

	getProveedor(id){
		return this._http.get(this.url+'proveedores/user/'+ id).map(res=> res.json());
	}
}