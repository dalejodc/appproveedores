import { Component } from '@angular/core';

@Component({
	selector: 'barraNavegacion',
	templateUrl: '../views/barraNavegacion.component.html',
	providers: []
})

export class BarraNavegacionComponent{
	private usuario: string;
	
	constructor(){
		this.usuario = sessionStorage.getItem("user");
	}

	ngOnInit(){
	}
}