import { Component } from '@angular/core';
import { SweetAlertService } from 'angular-sweetalert-service';

import { Repuesto } from '../models/repuesto'
import { RepuestosSV } from '../services/repuestos.service'

import { ProveedoresSV } from '../services/proveedores.service'

@Component({
	selector: 'repuestos',
	templateUrl: '../views/repuestos-list.component.html',
	providers: [RepuestosSV, ProveedoresSV]
})

export class RepuestosComponent{

	public listaRepuestos : Repuesto[];
	
	constructor(
		private _repuestoSV : RepuestosSV,
		private _proveedorSV : ProveedoresSV,
		private alertService: SweetAlertService
	){
		console.log("Componente de repuestos");
	}

	ngOnInit(){
		console.log(sessionStorage.getItem('user_id'));
		this.getProveedor();
	}

	getProveedor(){
		this._proveedorSV.getProveedor(parseInt(sessionStorage.getItem('user_id'))).subscribe(
			result => {
				console.log(result);
				sessionStorage.setItem('inv_id', result.inventario.id);
				console.log(sessionStorage.getItem('inv_id'));
				this.getRepuestos();
				// this.listaRepuestos = result;
			},
			error => {
				console.log(<any>error);
			}
		);
		
	}

	getRepuestos(){
		this._repuestoSV.getRepuestos(parseInt(sessionStorage.getItem('inv_id'))).subscribe(
			result => {
				console.log(result);
				this.listaRepuestos = result;
			},
			error => {
				console.log(<any>error);
			}
		);
	}

	eliminarRepuesto(id){
		// this._repuestoSV.deleteRepuesto(id).subscribe(
		// 		response =>{
		// 			this.ngOnInit();
		// 		},
		// 		error =>{
		// 			console.log(<any>error);
		// 			this.ngOnInit();
		// 		}
		// );
		this.alertService.confirm({
	      title: '¿Desea eliminar el repuesto?',
	      text: "El repuesto se eliminará, la acción no se puede revertir.",
	      cancelButtonColor: '#E57373',
  		  confirmButtonColor: '#83C283',
  		  confirmButtonText: 'Aceptar'
	    })
	    .then(() => {
			this._repuestoSV.deleteRepuesto(id).subscribe(
				response =>{
					this.ngOnInit();
				},
				error =>{
					console.log(<any>error);
					this.ngOnInit();
				}
		);
	      this.alertService.success({
	        title: '¡El repuesto fue eliminado exitosamente!'
	      });
	    })

	    .catch(() => console.log('canceled'));	
	}
}