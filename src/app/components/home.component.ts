import { Component } from '@angular/core';
import { BarraNavegacionComponent } from './barraNavegacion.component';

import { ProveedoresSV } from '../services/proveedores.service';


@Component({
	selector: 'home',
	templateUrl: '../views/home.component.html',
	providers:[ProveedoresSV]
})

export class HomeComponent{
	public titulo: string;

	constructor(
		private _proveedoresSV : ProveedoresSV,
	){
		this.titulo = '';
	}

	ngOnInit(){
		this.getProveedor();
	}

	getProveedor(){
		this._proveedoresSV.getProveedor(parseInt(sessionStorage.getItem('user_id'))).subscribe(
			response =>{
				console.log(response);
				sessionStorage.setItem('inv_id', response.inventario.id);
				this.titulo = response.nombre;
				sessionStorage.setItem('emp_nombre', response.nombre);
			},
			error =>{
				console.log(<any>error);
			}
		);
	}
}